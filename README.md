## Task Manager training project
### Screenshots

from IDE:

![screenshot of the application running in IDE](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-02/1_run_from_ide.png)

.jar:

![!screenshot of the application running from jar](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-02/2_jar.png)
